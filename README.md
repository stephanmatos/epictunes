# Epic Tunes Ripoff

# Api calls : 
* Select All Customers -- /api/customers/selectAll
* Select Count(customers) by country -- /api/customers/selectByCountryCount
* Select customers group by spending -- /api/customers/selectByTopSpending
* Select customers top genry by customerId -- api/customers/selectByCustomerByTopGenre/(yourDiseredcustomerId)
* Insert customer into Customers -- /api/customers/insert   -- Customer object in body
* Update customer into Customers -- /api/customers/update   -- Customer object in body

# Front end 
* Shows 5 random artist, tracks and genres -- /home 
* Search, returns every track containing the input string -- /search/?searchterm=(yourSearchString)


# Postman Collection Test
* https://www.getpostman.com/collections/61ef191241f47ed5224f

# Heroku app from git
* https://epic-tunes-dk1.herokuapp.com/

# Heroku app via docker
* https://epicdocker.herokuapp.com/