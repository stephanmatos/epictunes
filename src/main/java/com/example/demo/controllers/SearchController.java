package com.example.demo.controllers;

import com.example.demo.dbhelper.SqlSearchHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class SearchController {
    // Search controller
    // Returns the tracks found in from the inputstring

    @GetMapping(value = "/search")
    public String selectFiveRandomArtist(Model model, @RequestParam (value = "searchterm") String param){
        SqlSearchHelper sqlSearchHelper = new SqlSearchHelper();
        model.addAttribute("searchResults",sqlSearchHelper.searchForTracks(param));
        return "search";
    }

}
