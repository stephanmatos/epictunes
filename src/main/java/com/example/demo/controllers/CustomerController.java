package com.example.demo.controllers;

import com.example.demo.dbhelper.SqlAPIHelper;
import com.example.demo.models.Customer;
import com.example.demo.models.CustomerCountry;
import com.example.demo.models.CustomerSpending;
import com.example.demo.models.CustomerTopGenre;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@RestController
@RequestMapping ("/api/customers")
public class CustomerController {
    SqlAPIHelper helper = new SqlAPIHelper();

    // Api customer controller. This "class" handles all the customer api calls.
    

    @RequestMapping ("/selectAll")
    public ArrayList<Customer> selectAllCustomers(){
        return helper.selectAllCustomers();
    }
    @RequestMapping("/selectByCountryCount")
    public ArrayList<CustomerCountry> selectGroupByCountry(){

        return helper.selectGroupByCountry();
    }

    @RequestMapping("/selectByTopSpending")
    public ArrayList<CustomerSpending> selectGroupBySpending(){

        return helper.selectCustomerBySpending();
    }
    @PutMapping("/update")
    public String updateCustomer(@RequestBody Customer customer){
        if(helper.updateCustomer(customer)){
            return "Customer was updated successfully";
        }
        return "Customer failed to update";
    }
    @PostMapping("/insert")
    public String insertCustomer(@RequestBody Customer customer){
        if(helper.insertCustomer(customer)){
            return "Customer was inserted successfully";
        }
        return "Insert Customer failed";
    }
    @RequestMapping("/selectByCustomerByTopGenre/{id}")
    public CustomerTopGenre selectByCustomerByTopGenre(@PathVariable String id){
        return helper.selectCustomerTopGenre(id);
    }
}
