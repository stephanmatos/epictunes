package com.example.demo.controllers;

import com.example.demo.dbhelper.SqlRandom;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class HomeController {
    SqlRandom randomHelper = new SqlRandom();

    // Thymeleaf home controller - returns 5 random tracks, artist and genres on update

    @GetMapping(value = "/home")
    public String selectFiveRandomHome(Model model){
        model.addAttribute("randomArtist",randomHelper.selectFiveRandomArtist());
        model.addAttribute("randomTrack",randomHelper.selectFiveRandomTracks());
        model.addAttribute("randomGenre",randomHelper.selectFiveRandomGenres());
        return "index";
    }

    @GetMapping(value = "/")
    public String selectFiveRandom(Model model){
        model.addAttribute("randomArtist",randomHelper.selectFiveRandomArtist());
        model.addAttribute("randomTrack",randomHelper.selectFiveRandomTracks());
        model.addAttribute("randomGenre",randomHelper.selectFiveRandomGenres());
        return "index";
    }


}
