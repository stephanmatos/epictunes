package com.example.demo.dbhelper;

import com.example.demo.models.Search;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class SqlSearchHelper {
    // Setup

    String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    Connection conn = null;

    // Returns all tracks that contains the input string
    public ArrayList<Search> searchForTracks(String searchString){
        ArrayList<Search> searchesArrayList = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(URL);
            PreparedStatement ps = conn.prepareStatement("Select Track.Name as trackName, A.Title as albumTitle ,A2.Name as artistName, G.Name as genreName from Track inner join Album A on Track.AlbumId = A.AlbumId inner join\n" +
                    "    Artist A2 on A.ArtistId = A2.ArtistId inner join Genre G on Track.GenreId = G.GenreId where Track.Name LIKE ?");
            ps.setString(1,"%"+searchString+"%");
            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next()){
                searchesArrayList.add(new Search(resultSet.getString("trackName"),
                        resultSet.getString("albumTitle"),resultSet.getString("artistName"),
                        resultSet.getString("genreName")));
            }
            conn.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return searchesArrayList;
    }
}
