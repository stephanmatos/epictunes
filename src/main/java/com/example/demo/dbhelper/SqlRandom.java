package com.example.demo.dbhelper;

import com.example.demo.models.Artist;
import com.example.demo.models.Genre;
import com.example.demo.models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;


public class SqlRandom {
    // Setup
    String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    Connection conn = null;

    // Selects five randoms tracks for the front page
    public ArrayList<Track> selectFiveRandomTracks(){
        ArrayList<Track> trackArrayList = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(URL);

            PreparedStatement ps = conn.prepareStatement("select TrackId,Name from Track ORDER BY RANDOM() limit 5");
            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next()){
                trackArrayList.add(new Track(resultSet.getString("TrackId"),resultSet.getString("Name")));
            }

            conn.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return trackArrayList;
    }

    // Selects five randoms genres for the front page
    public ArrayList<Genre> selectFiveRandomGenres(){
        ArrayList<Genre> genreArrayList = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(URL);

            PreparedStatement ps = conn.prepareStatement("select GenreId,Name from Genre order by random() limit 5");
            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next()){
                genreArrayList.add(new Genre(resultSet.getString("GenreId"),resultSet.getString("Name")));
            }

            conn.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return genreArrayList;
    }

    // Selects five randoms artist for the front page
    public ArrayList<Artist> selectFiveRandomArtist(){

        ArrayList<Artist> artistArrayList = new ArrayList<>();

        try{
            conn = DriverManager.getConnection(URL);

            PreparedStatement ps = conn.prepareStatement("select ArtistId, Name from Artist order by random() limit 5");
            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next()){
                artistArrayList.add(new Artist(Integer.parseInt(resultSet.getString("ArtistId")),resultSet.getString("Name")));
            }

            conn.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return artistArrayList;
    }
}
