package com.example.demo.dbhelper;

import com.example.demo.models.*;

import java.sql.*;
import java.util.ArrayList;


public class SqlAPIHelper {

    // Setup

    String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    Connection conn = null;

    // Select All customers with and return an arraylist with customer type object inside
    public ArrayList<Customer> selectAllCustomers(){
        ArrayList<Customer> customers = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);

            PreparedStatement ps = conn.prepareStatement("Select CustomerId, FirstName,LastName,Country,PostalCode,Phone,Email FROM Customer");
            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next()){
                Customer customer = new Customer(resultSet.getString("CustomerId"),resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),resultSet.getString("Country"),
                        resultSet.getString("PostalCode"), resultSet.getString("Phone"),resultSet.getString("Email"));
                customers.add(customer);
            }

            conn.close();
        } catch (Exception exception) {
            System.out.println("Something went wrong...");
            System.out.println(exception.toString());
        }

        return customers;
    }


    // Insert a customer to the database. Will take a customer object as input and put it into the database.
    // Customer id is a self incrementing int and wil generate it self
    // Returns true if insert customer was successful
    public boolean insertCustomer(Customer customer){
        try {
            conn = DriverManager.getConnection(URL);

            PreparedStatement ps = conn.prepareStatement("Insert into Customer(FirstName,LastName,Country,PostalCode,Phone,Email) values (?,?,?,?,?,?)");
            ps.setString(1, customer.getFirstName());
            ps.setString(2,customer.getLastName());
            ps.setString(3,customer.getCountry());
            ps.setString(4,customer.getPostalCode());
            ps.setString(5,customer.getPhone());
            ps.setString(6,customer.getEmail());
            ps.execute();
            conn.close();

        } catch (Exception exception) {
            System.out.println("Something went wrong...");
            System.out.println(exception.toString());
            return false;
        }
        return true;
    }

    // Update customer in the database. Takes customer object as input and returns true if the customer
    // was successfully updated
    public boolean updateCustomer(Customer customer){

        try {

            conn = DriverManager.getConnection(URL);

            PreparedStatement ps = conn.prepareStatement("update Customer set FirstName = ?, LastName = ?, " +
                    "Country = ?, PostalCode = ?, Phone = ?, email = ? where CustomerId = ? ");

            ps.setString(1, customer.getFirstName());
            ps.setString(2,customer.getLastName());
            ps.setString(3,customer.getCountry());
            ps.setString(4,customer.getPostalCode());
            ps.setString(5,customer.getPhone());
            ps.setString(6,customer.getEmail());
            ps.setString(7,customer.getCompanyId());
            ps.execute();
            conn.close();

        } catch (Exception exception) {
            System.out.println("Something went wrong...");
            System.out.println(exception.toString());
            return false;
        }

        return true;
    }

    // returns how many customers that are in the database grouped by country.
    // An arraylist is returned with customerCountry objects in

    public ArrayList<CustomerCountry> selectGroupByCountry(){
        ArrayList<CustomerCountry> customerCountryArrayList = new ArrayList<>();
        try{
            conn = DriverManager.getConnection(URL);

            PreparedStatement ps = conn.prepareStatement("Select count(CustomerId), Country from Customer GROUP by Country Order by count(CustomerId) desc");
            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next()){
                String cc = resultSet.getString("count(CustomerId)");
                customerCountryArrayList.add(new CustomerCountry(resultSet.getString("Country"),cc));
            }

         conn.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        return customerCountryArrayList;
    }

    // Returns an arraylist with customerSpending objects.
    // This object contains an id, a name and its amount spent in the app.

    public ArrayList<CustomerSpending> selectCustomerBySpending(){
        ArrayList<CustomerSpending> customerSpendingArrayList = new ArrayList<>();
        try{
            conn = DriverManager.getConnection(URL);

            PreparedStatement ps = conn.prepareStatement("select sum(Total), FirstName, C.CustomerId from Invoice " +
                    "inner join Customer C on Invoice.CustomerId = C.CustomerId " +
                    "group by C.CustomerId Order by sum(Total) desc");
            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next()){
                customerSpendingArrayList.add(new CustomerSpending(resultSet.getString("CustomerId"),resultSet.getString("FirstName"),resultSet.getString("sum(Total)")));

            }

            conn.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        return customerSpendingArrayList;
    }

    // This returns a customers top genre based on the amount of tracks it has bought.

    public CustomerTopGenre selectCustomerTopGenre(String id){

        CustomerTopGenre customerTopGenre = null;
        ArrayList<Genre> genres = new ArrayList<>();
        try{
            conn = DriverManager.getConnection(URL);

            PreparedStatement ps = conn.prepareStatement("SELECT CustomerId,FirstName, genre , GenreId , sum FROM (SELECT Customer.CustomerId, Customer.FirstName ,Genre.Name as genre,Genre.GenreId, COUNT(Genre.Name) AS sum FROM Customer INNER JOIN Invoice ON Invoice.CustomerId = Customer.CustomerId INNER JOIN InvoiceLine ON InvoiceLine.InvoiceId = Invoice.InvoiceId INNER JOIN Track ON Track.TrackId = InvoiceLine.TrackId INNER JOIN Genre ON Genre.GenreId = Track.GenreId WHERE Customer.CustomerId = ? GROUP BY Genre.Name ORDER BY sum DESC) WHERE sum = (SELECT MAX(sum) FROM (SELECT Genre.Name, COUNT(Genre.Name) AS sum FROM Customer INNER JOIN Invoice ON Invoice.CustomerId = Customer.CustomerId INNER JOIN InvoiceLine ON InvoiceLine.InvoiceId = Invoice.InvoiceId INNER JOIN Track ON Track.TrackId = InvoiceLine.TrackId INNER JOIN Genre ON Genre.GenreId = Track.GenreId WHERE Customer.CustomerId = ? GROUP BY Genre.Name ORDER BY sum DESC))");
            ps.setString(1,id);
            ps.setString(2,id);

            ResultSet resultSet = ps.executeQuery();
            String name = resultSet.getString("FirstName");
            String customerId = resultSet.getString("CustomerId");
            String count = resultSet.getString("sum");

            while(resultSet.next()){
                genres.add(new Genre(resultSet.getString("GenreId"),resultSet.getString("Genre")));
            }

            customerTopGenre = new CustomerTopGenre(customerId,name,genres,count);

            conn.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }



       return customerTopGenre;

    }

}
