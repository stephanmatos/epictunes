package com.example.demo.models;

public class Track {

    private String trackId;
    private String name;


    public Track(String trackId, String name) {
        this.trackId = trackId;
        this.name = name;
    }

    public void printTrack(){

        String output = String.format("Track id :  %s ---- Track name : %s ", trackId,name);
        System.out.println(output);
    }
    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
