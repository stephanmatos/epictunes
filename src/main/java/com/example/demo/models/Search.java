package com.example.demo.models;

public class Search {

    private String trackName;
    private String albumTitle;
    private String artistName;
    private String genreName;

    public Search(String trackName, String albumTitle, String artistName, String genreName) {
        this.trackName = trackName;
        this.albumTitle = albumTitle;
        this.artistName = artistName;
        this.genreName = genreName;
    }
    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }
}
