package com.example.demo.models;

public class CustomerSpending {
    private String id;
    private String name;
    private String amountSpend;

    public CustomerSpending(String id, String name, String amountSpend){
        this.id = id;
        this.name = name;
        this.amountSpend = amountSpend;

    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmountSpend() {
        return amountSpend;
    }

    public void setAmountSpend(String amountSpend) {
        this.amountSpend = amountSpend;
    }
}
