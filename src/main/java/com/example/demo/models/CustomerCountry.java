package com.example.demo.models;

public class CustomerCountry {

    private String customersInCountry;
    private String country;


    public CustomerCountry(String country, String customersInCountry){
        this.country = country;
        this.customersInCountry = customersInCountry;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCustomersInCountry() {
        return customersInCountry;
    }

    public void setCustomersInCountry(String customersInCountry) {
        this.customersInCountry = customersInCountry;
    }
}
