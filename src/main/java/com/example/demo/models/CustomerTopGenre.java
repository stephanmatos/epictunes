package com.example.demo.models;

import java.util.ArrayList;

public class CustomerTopGenre {

    private String customerId;
    private String name;
    private ArrayList<Genre> genres;
    private String topGenreCount;

    public CustomerTopGenre(String customerId, String name, ArrayList<Genre> genres, String topGenreCount) {
        this.customerId = customerId;
        this.name = name;
        this.genres = genres;
        this.topGenreCount = topGenreCount;
    }

    public String getTopGenreCount() {
        return topGenreCount;
    }

    public void setTopGenreCount(String topGenreCount) {
        this.topGenreCount = topGenreCount;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Genre> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<Genre> genres) {
        this.genres = genres;
    }
}
