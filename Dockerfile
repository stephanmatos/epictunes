FROM openjdk:15
ADD target/demo-2.3.4.jar epic-tunes-dk.jar
ENTRYPOINT ["java", "-jar","epic-tunes-dk.jar"]